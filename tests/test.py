"""
Unit testing of the automatic batch processing application
"""
import unittest
from src.script import calculate_degree


class AppTests(unittest.TestCase):
    def test_app(self):
        """Simple Tests"""
        self.assertEqual(calculate_degree(5, 2), 25)
        self.assertNotEqual(calculate_degree(2, 2), 5)

    def test_errors(self):
        """Check that method fails when parameter type is not numeric"""
        with self.assertRaises(TypeError):
            calculate_degree("foo", 10)


def suite():
    _suite = unittest.TestSuite()
    _suite.addTest(AppTests('test_app'))
    _suite.addTest(AppTests('test_errors'))
    return _suite