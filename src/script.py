from math import *

def my_function(x):
    return exp(sin(sqrt(fabs(x))) + 1) # Наша функция будет четной,
    # Извлекаем корень квадратный,
    # Берем синус, прибавляем 1, а затем это выражение сразу в показатель экспоненты
print(my_function(1000))

###########################

def calculate_degree(x: int, y):
    y = x ** y
    return y


def custom_sum(x, y):
    return x + y


def custom_function():
    print("Hello world")


if __name__ == "__main__":
    print(f"The square of 10 is: {calculate_degree(10, 3)}")
    custom_sum(5, 10)
    custom_function()
