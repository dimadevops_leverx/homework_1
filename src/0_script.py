def calculate_degree(x: int, y):
    y = x ** y
    return y


def custom_sum(x, y):
    return x + y


def custom_function():
    print("Hello world")


if __name__ == "__main__":
    print(f"The square of 10 is: {calculate_degree(10, 3)}")
    custom_sum(5, 10)
    custom_function()
